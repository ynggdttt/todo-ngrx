import { Component, OnInit } from '@angular/core';
import * as fromFilter from '../filter/filter.actions';
import {SetFilterAction} from '../filter/filter.actions';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.reducers';
import {Todo} from '../model/todo.model';
import {DeleteAllTodoAction} from '../todo.actions';

@Component({
  selector: 'app-todo-footer',
  templateUrl: './todo-footer.component.html',
  styles: []
})
export class TodoFooterComponent implements OnInit {
  public filterValid: fromFilter.filterValid [] = ['ALL', 'ACTIVE', 'COMPLETED'];
  public filterSelected: fromFilter.filterValid;
  public todosPending: number;
  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.store.subscribe(state => {
      this.filterSelected = state.filter;
      this.countTodosPending(state.todos);
    });
  }

  public changeFilter(filterValida: fromFilter.filterValid): void {
    const action = new SetFilterAction(filterValida);
    this.store.dispatch(action);
  }

  public countTodosPending(todos: Todo[]): void {
    this.todosPending = todos.filter(todo => !todo.completed ).length;
  }

  public deleteAll(): void {
    const action = new DeleteAllTodoAction();
    this.store.dispatch(action);
  }

}
