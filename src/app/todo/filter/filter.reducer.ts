import * as fromFilter from './filter.actions';

const stateStart: fromFilter.filterValid = 'ALL';

export function filterReducer(state = stateStart, action: fromFilter.actions): fromFilter.filterValid {

    switch (action.type) {
        case fromFilter.SET_FILTER:
            return action.filter;
        default:
            return state;
    }

}