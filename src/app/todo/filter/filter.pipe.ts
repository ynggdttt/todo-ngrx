import { Pipe, PipeTransform } from '@angular/core';
import {Todo} from '../model/todo.model';
import * as fromFilter from './filter.actions';

@Pipe({
  name: 'filterTodo'
})
export class FilterPipe implements PipeTransform {

  transform(todos: Todo[], filter: fromFilter.filterValid): any {
    switch (filter) {
        case 'COMPLETED':
          return todos.filter(todo => todo.completed);
        case 'ACTIVE':
          return todos.filter(todo => !todo.completed);
        default:
          return todos;
    }
  }

}
